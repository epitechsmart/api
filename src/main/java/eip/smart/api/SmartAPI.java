package eip.smart.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;

import eip.smart.cscommons.model.ServerStatus;
import eip.smart.cscommons.model.agent.Agent;
import eip.smart.cscommons.model.geometry.Point3D;
import eip.smart.cscommons.model.modeling.Area;
import eip.smart.cscommons.model.modeling.Modeling;

public class SmartAPI<T> {

	/**
	 * Class used to transform a low-level response to objects.
	 *
	 * @author Pierre Demessence
	 *
	 * @param <T>
	 *            Type of the data returned after transformation.
	 */
	private interface SmartAPIInnerCallback<T> {
		/**
		 * Should be implemented to tranform the JSON data contained into the low-level response into objets.
		 *
		 * @param res
		 *            The low-level response.
		 * @return The data in the form of Java objects.
		 */
		public abstract T onPreSuccess(SmartAPIResponse res);
	}

	private static SmartAPICallback<ServerStatus>	globalCallback;

	// public static String SERVER_URL = "http://54.148.17.11:8080/smartserver/";
	/**
	 * The URL of the Server. Will be used for each request and can be modified at any time.
	 */
	public static String							SERVER_URL	= "http://localhost:8080/smartserver/";

	/**
	 * Add an area to the current modeling.
	 *
	 * @param area
	 *            The area to add.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> addArea(Area area) {
		return (new SmartAPI<>(new SmartAPIRequest("add_area").addData("area", area), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Give a manual order to the Agent. This order will be added to the orders list of the agent.
	 *
	 * @param name
	 *            The name of the agent whom to give order.
	 * @param order
	 *            The 3D point where the Agent should go.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> AddWayPoint(String name, Point3D order) {
		return (new SmartAPI<>(new SmartAPIRequest("add_way_point").addData("name", name).addData("order", order), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Add an agent to the current modeling.
	 *
	 * @param name
	 *            The agent to add.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> agentAdd(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("agent_add").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Delete and disconnect an agent from the server.
	 *
	 * @param name
	 *            The agent to delete.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> agentDelete(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("agent_delete").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Recall an agent to his starting point.
	 *
	 * @param name
	 *            The agent to recall.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> agentRecall(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("agent_recall").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Remove an agent from the current modeling.
	 *
	 * @param name
	 *            The agent to remove.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> agentRemove(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("agent_remove").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Clean the server, removing all saved modelings, the current modeling and removing all agents whether they are connected or disconnected.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> cleanServer() {
		return (new SmartAPI<>(new SmartAPIRequest("clean_server"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Create a new configuration file.
	 *
	 * @param name
	 *            The name of the configuration to create.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> confCreate(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_create").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Delete a configuration file.
	 *
	 * @param name
	 *            The name of the configuration to delete.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> confDelete(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_delete").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Delete a key from a configuration file.
	 *
	 * @param name
	 *            The name of the configuration to delete.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> confDeleteKey(String name, String key) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_delete_key").addData("name", name).addData("key", key), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Return all the pair of key-value of a configuration.
	 *
	 * @param name
	 *            The name of the configuration to dump.
	 * @return The dumping.
	 */
	public static SmartAPI<HashMap<String, String>> confDump(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_dump").addData("name", name), new SmartAPIInnerCallback<HashMap<String, String>>() {
			@Override
			public HashMap<String, String> onPreSuccess(SmartAPIResponse res) {
				HashMap<String, String> dump = new HashMap<>();
				JsonNode conf = res.getData().get("conf");
				for (int i = 0; i < conf.size(); i++) {
					String field = conf.get(i).fieldNames().next();
					dump.put(field, conf.get(i).get(field).asText());
				}
				return (dump);
			}
		}));
	}

	/**
	 * Get the value of one of the properties in a configuration.
	 *
	 * @param name
	 *            The name of the configuration.
	 * @param key
	 *            The key of the property.
	 * @return The value of the property.
	 */
	public static SmartAPI<String> confGet(String name, String key) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_get").addData("name", name).addData("key", key), new SmartAPIInnerCallback<String>() {
			@Override
			public String onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONObject(String.class, res.getData().findValue("value")));
			}
		}));
	}

	/**
	 * Get a list of the keys of all the the properties in a configuration
	 *
	 * @param name
	 *            The name of the configuration.
	 * @return The list of the keys.
	 */
	public static SmartAPI<List<String>> confKeys(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_keys").addData("name", name), new SmartAPIInnerCallback<List<String>>() {
			@Override
			public List<String> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(String.class, res.getData().findValue("keys")));
			}
		}));
	}

	/**
	 * Get the name of all the configurations in the server
	 *
	 * @return A list of all the configurations in the server.
	 */
	public static SmartAPI<List<String>> confList() {
		return (new SmartAPI<>(new SmartAPIRequest("conf_list"), new SmartAPIInnerCallback<List<String>>() {
			@Override
			public List<String> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(String.class, res.getData().findValue("configurations")));
			}
		}));
	}

	/**
	 * Set a property value in a configuration.
	 *
	 * @param name
	 *            The name of the configuration.
	 * @param key
	 *            The key of the property to set.
	 * @param value
	 *            The value to set
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> confSet(String name, String key, String value) {
		return (new SmartAPI<>(new SmartAPIRequest("conf_set").addData("name", name).addData("key", key).addData("value", value), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Create some testing agents. Their name will be "TestAgent#X" where X is an unique number.
	 *
	 * @param nb
	 *            The number of testing agents to create.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> createTestAgents(int nb) {
		return (new SmartAPI<>(new SmartAPIRequest("create_test_agents").addData("nb", nb), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Delete all the testing agents. This is based on the name so every agent whose name begin with "TestAgent#" will be deleted.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> deleteTestAgents() {
		return (new SmartAPI<>(new SmartAPIRequest("delete_test_agents"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Get info about an agent.
	 *
	 * @param name
	 *            The name of the agent to retrieve.
	 * @return The Agent object containing all the data.
	 */
	public static SmartAPI<Agent> getAgentInfo(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("get_agent_info").addData("name", name), new SmartAPIInnerCallback<Agent>() {
			@Override
			public Agent onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONObject(Agent.class, res.getData().findValue("agent")));
			}
		}));
	}

	/**
	 * Get the list of the agents associated with the current modeling.
	 *
	 * @return An List of SimpleAgentProxy containing simplified data of the current working agents.
	 */
	public static SmartAPI<List<Agent>> getAgents() {
		return (new SmartAPI<>(new SmartAPIRequest("get_agents"), new SmartAPIInnerCallback<List<Agent>>() {
			@Override
			public List<Agent> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(Agent.class, res.getData().findValue("agents")));
			}
		}));
	}

	/**
	 * Get the list of all the available agents.
	 *
	 * @return An List of SimpleAgentProxy containing simplified data of the available agents.
	 */
	public static SmartAPI<List<Agent>> getAgentsAvailable() {
		return (new SmartAPI<>(new SmartAPIRequest("get_agents_available"), new SmartAPIInnerCallback<List<Agent>>() {
			@Override
			public List<Agent> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(Agent.class, res.getData().findValue("agents")));
			}
		}));
	}

	/**
	 * Get the a point-cloud of the whole current modeling
	 *
	 * @return An List of the points that represent the modeling.
	 */
	public static SmartAPI<List<Point3D>> getPoints() {
		return (new SmartAPI<>(new SmartAPIRequest("get_points"), new SmartAPIInnerCallback<List<Point3D>>() {

			@Override
			public List<Point3D> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(Point3D.class, res.getData().findValue("pointcloud").findValue("points")));
			}
		}));
	}

	/**
	 * Get the list of possible Status.
	 *
	 * @return An List of the possible Status.
	 */
	public static SmartAPI<List<ServerStatus>> listStatus() {
		return (new SmartAPI<>(new SmartAPIRequest("list_status"), new SmartAPIInnerCallback<List<ServerStatus>>() {
			@Override
			public List<ServerStatus> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(ServerStatus.class, res.getData().findValue("statuses")));
			}
		}));
	}

	/**
	 * Give a manual order to the Agent. This order will have the highest priority.
	 *
	 * @param name
	 *            The name of the agent whom to give order.
	 * @param order
	 *            The 3D point where the Agent should go.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> manualOrder(String name, Point3D order) {
		return (new SmartAPI<>(new SmartAPIRequest("manual_order").addData("name", name).addData("order", order), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	public static SmartAPI<ServerStatus> modelingCopy(String name, String copy) {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_copy").addData("name", name).addData("copy", copy), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Create a new Modeling. This modeling will be the current modeling and thus can only be done if there is no current modeling.
	 *
	 * @param name
	 *            The name of the new modeling.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingCreate(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_create").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Delete the stored given modeling.
	 *
	 * @param name
	 *            The name of the modeling to delete.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingDelete(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_delete").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Get info about the current modeling.
	 *
	 * @return A Modeling object containing all the data about the current modeling.
	 */
	public static SmartAPI<Modeling> modelingInfo() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_info"), new SmartAPIInnerCallback<Modeling>() {
			@Override
			public Modeling onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONObject(Modeling.class, res.getData().findValue("modeling")));
			}
		}));
	}

	/**
	 * Get the list of all the stored modelings.
	 *
	 * @return An List of SimpleModelingProxy containing simplified data of the stored modelings.
	 */
	public static SmartAPI<List<Modeling>> modelingList() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_list"), new SmartAPIInnerCallback<List<Modeling>>() {
			@Override
			public List<Modeling> onPreSuccess(SmartAPIResponse res) {
				return (SmartAPI.parseJSONArray(Modeling.class, res.getData().findValue("modelings")));
			}
		}));
	}

	/**
	 * Load the given modeling and make it the current modeling.
	 *
	 * @param name
	 *            The name of the modeling to load.
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingLoad(String name) {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_load").addData("name", name), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Pause the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingPause() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_pause"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Resume the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingResume() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_resume"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Save the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingSave() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_save"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Start the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingStart() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_start"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Stop and save the current modeling and remove it from being the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingStop() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_stop"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Unload the current modeling.
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> modelingUnload() {
		return (new SmartAPI<>(new SmartAPIRequest("modeling_unload"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	/**
	 * Transform a JsonNode containing multiple objects of the same type into an List of these objects.
	 *
	 * @param theClass
	 *            The type of the contained objects.
	 * @param JSONarray
	 *            The JsonNode containing the data.
	 * @return An List of the parsed objects.
	 */
	private static <E> List<E> parseJSONArray(Class<E> theClass, JsonNode JSONarray) {
		List<E> array = new ArrayList<>();
		for (int i = 0; i < JSONarray.size(); i++)
			array.add(SmartAPI.parseJSONObject(theClass, JSONarray.get(i)));
		return (array);
	}

	/**
	 * Transform a JsonNode containing an single data into an object.
	 *
	 * @param theClass
	 *            The type of the contained object.
	 * @param JSONObject
	 *            The JsonNode containing the data.
	 * @return The parsed object.
	 */
	private static <E> E parseJSONObject(Class<E> theClass, JsonNode JSONObject) {
		E object = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			object = mapper.readValue(JSONObject.toString(), theClass);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (object);
	}

	/**
	 * Set a global high-level callback that will be called for each available request.
	 *
	 * @param globalCallback
	 *            The global callback.
	 */
	public static void setGlobalCallback(SmartAPICallback<ServerStatus> globalCallback) {
		SmartAPI.globalCallback = globalCallback;
	}

	/**
	 * Set the connection timeout and socket timeout
	 *
	 * @param connectionTimeout
	 *            The timeout until a connection with the server is established (in milliseconds).
	 *            Default is 10000. Set to zero to disable the timeout.
	 * @param socketTimeout
	 *            The timeout until a connection with the server is established (in milliseconds).
	 *            Default is 10000. Set to zero to disable the timeout.
	 */
	public static void setTimeouts(long connectionTimeout, long socketTimeout) {
		Unirest.setTimeouts(connectionTimeout, socketTimeout);
	}

	public static void shutdownExecutor() {
		SmartAPIRequest.shutdownExecutor();
	}

	/**
	 * Restart the TCP and UDP sockets
	 *
	 * @return The Status returned by the server.
	 */
	public static SmartAPI<ServerStatus> socketRestart() {
		return (new SmartAPI<>(new SmartAPIRequest("socket_restart"), new SmartAPIInnerCallback<ServerStatus>() {
			@Override
			public ServerStatus onPreSuccess(SmartAPIResponse res) {
				return (res.getStatus());
			}
		}));
	}

	private SmartAPIInnerCallback<T>	preCallback;

	private SmartAPIRequest				request;

	/**
	 *
	 * @param request
	 *            The low-level request.
	 * @param preCallback
	 *            The pre-callback used to transform a low-level response into objects.
	 */
	private SmartAPI(SmartAPIRequest request, SmartAPIInnerCallback<T> preCallback) {
		this.request = request;
		this.request.setServerUrl(SmartAPI.SERVER_URL);
		this.preCallback = preCallback;
	}

	/**
	 * Transform a high-level callback into a low-level callback.
	 *
	 * @param callback
	 *            The high-level callback class given by the user.
	 * @return SmartAPIRequestCallback The low-level callback to pass to the request.
	 */
	private SmartAPIRequestCallback doRequestCallback(SmartAPICallback<T> callback) {
		return (new SmartAPIRequestCallback() {

			@Override
			public void onFail(Exception e) {
				if (callback != null)
					callback.onFail(e);
				if (SmartAPI.globalCallback != null)
					SmartAPI.globalCallback.onFail(e);
			}

			@Override
			public void onSuccess(SmartAPIResponse res) {
				if (res.getStatus().getCode() > 0) {
					if (callback != null)
						callback.onError(res.getStatus());
					if (SmartAPI.globalCallback != null)
						SmartAPI.globalCallback.onError(res.getStatus());
				} else {
					if (callback != null)
						callback.onSuccess(SmartAPI.this.preCallback.onPreSuccess(res));
					if (SmartAPI.globalCallback != null)
						SmartAPI.globalCallback.onSuccess(res.getStatus());
				}
			}
		});
	}

	/**
	 * Run the request synchronously.
	 *
	 * @param callback
	 *            The Callback class to implement.
	 */
	public void run(SmartAPICallback<T> callback) {
		this.request.run(this.doRequestCallback(callback));
	}

	/**
	 * Run the request asynchronously
	 *
	 * @param callback
	 *            The Callback class to implement.
	 */
	public void runAsync(SmartAPICallback<T> callback) {
		this.request.runAsync(this.doRequestCallback(callback));
	}

}
