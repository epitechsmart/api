package eip.smart.api;

import eip.smart.cscommons.model.ServerStatus;

/**
 * Callback class to get the result after doing a request to the Server.
 *
 * @author Pierre Demessence
 *
 * @param <T>
 *            The type of data returned in case of success.
 */
public interface SmartAPICallback<T> {
	/**
	 * Callback called when a logical error occurs after reaching the server.
	 * For example deleting a non existing Modeling.
	 *
	 * @param s
	 *            The Status returned by the server.
	 */
	public void onError(ServerStatus s);

	/**
	 * Callback called when an Exception occurs when reaching the server.
	 *
	 * @param e
	 *            The Exception thrown by Java.
	 */
	public void onFail(Exception e);

	/**
	 * Callback called in case of success.
	 *
	 * @param t
	 *            The data asked. Will always be a Status of 200/OK if there is no data to retrieve.
	 */
	public void onSuccess(T t);
}
