package eip.smart.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import eip.smart.cscommons.model.JSONViews;
import eip.smart.cscommons.util.WrapperTypes;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SmartAPIRequest {

	public static String					DEFAULT_SERVER_URL	= "http://localhost:8080/smartserver/";
	private static final ExecutorService	EXECUTOR			= Executors.newFixedThreadPool(10, new BasicThreadFactory.Builder().namingPattern("SMARTAPI-pool-thread-%d").build());

	public static boolean					FORCE_SYNCHRONOUS	= false;

	public static void setTimeouts(long connectionTimeout, long socketTimeout) {
		Unirest.setTimeouts(connectionTimeout, socketTimeout);
	}

	public static void shutdownExecutor() {
		SmartAPIRequest.EXECUTOR.shutdownNow();
	}

	private HashMap<String, Object>	data;
	private ObjectMapper			mapper		= new ObjectMapper();
	private String					request;
	private String					serverUrl	= SmartAPIRequest.DEFAULT_SERVER_URL;

	public SmartAPIRequest(String request) {
		this(request, new HashMap<>());
	}

	public SmartAPIRequest(String request, HashMap<String, Object> data) {
		this.mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
		this.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		this.mapper.configure(MapperFeature.AUTO_DETECT_GETTERS, false);
		this.mapper.configure(MapperFeature.AUTO_DETECT_IS_GETTERS, false);
		this.request = request;
		this.data = data;
	}

	public SmartAPIRequest addData(String key, Object value) {
		this.data.put(key, value);
		return (this);
	}

	public String getUrl() {
		if (!this.serverUrl.endsWith("/"))
			this.serverUrl += "/";
		if (!(this.serverUrl.startsWith("http://") || this.serverUrl.startsWith("https://")))
			this.serverUrl = "http://" + this.serverUrl;
		return (this.serverUrl + this.request);
	}

    private GetRequest prepareRequest() throws JsonProcessingException {
        GetRequest req = Unirest.get(this.getUrl()).header("accept", "application/json");
        Iterator<Entry<String, Object>> iterator = this.data.entrySet().iterator();
        Entry<String, Object> entry;
        while (iterator.hasNext()) {
            entry = iterator.next();
            if (entry.getValue().getClass().isPrimitive() || WrapperTypes.isWrapperType(entry.getValue().getClass()) || entry.getValue().getClass() == String.class)
                req.queryString(entry.getKey(), entry.getValue());
            else
                req.queryString(entry.getKey(), this.mapper.writerWithView(JSONViews.HTTP.class).writeValueAsString(entry.getValue()));
        }
        return req;
    }

	public void run(SmartAPIRequestCallback callback) {
		try {
			GetRequest req = prepareRequest();
			HttpResponse<JsonNode> response = req.asJson();
			if (response.getStatus() != 200 && callback != null)
				callback.onFail(new Exception(response.getStatusText()));
			else if (callback != null)
				callback.onSuccess(new SmartAPIResponse(this, response.getRawBody()));
		} catch (Exception e) {
			callback.onFail(e);
		}
	}


	public Future<HttpResponse<JsonNode>> runAsync(SmartAPIRequestCallback callback) {
		if (SmartAPIRequest.FORCE_SYNCHRONOUS) {
            this.run(callback);
            return null;
        } else {
            GetRequest req = null;

            try {
                req = prepareRequest();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }

            Future<HttpResponse<JsonNode>> futureResponse = req.asJsonAsync(new Callback<JsonNode>() {
                @Override
                public void completed(HttpResponse<JsonNode> response) {
                    if (response.getStatus() != 200 && callback != null)
                        callback.onFail(new Exception(response.getStatusText()));
                    else if (callback != null)
                        callback.onSuccess(new SmartAPIResponse(SmartAPIRequest.this, response.getRawBody()));
                }

                @Override
                public void failed(UnirestException e) {
                    if (callback != null)
                        callback.onFail(new Exception(e.getMessage()));
                }

                @Override
                public void cancelled() {}
            });

            return futureResponse;
        }
    }

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

}
