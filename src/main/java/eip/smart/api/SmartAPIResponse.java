package eip.smart.api;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eip.smart.cscommons.model.ServerStatus;

public class SmartAPIResponse {
	private JsonNode		body;
	private SmartAPIRequest	request;

	public SmartAPIResponse(SmartAPIRequest request, InputStream inputStream) {
		this.request = request;
		try {
			this.body = new ObjectMapper().readTree(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public JsonNode getData() {
		return (this.body.findValue("data"));
	}

	public ServerStatus getStatus() {
		ServerStatus status = ServerStatus.getStatusByCode(this.body.findValue("status").findValue("code").asInt());
		status.setMessage(this.body.findValue("status").findValue("message").asText());
		return (status);
	}

	public String getUrl() {
		return this.request.getUrl();
	}
}
