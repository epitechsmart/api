package eip.smart.api;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.Unirest;

import eip.smart.cscommons.model.ServerStatus;

@SuppressWarnings("static-method")
public class SmartAPIConfTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SmartAPI.SERVER_URL = "http://localhost:8080/smartserver/";
		Unirest.setTimeouts(1000, 1000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Before
	public void setUp() throws Exception {}

	@Test
	public void simpleTest() {
		SmartAPI.confList().run(new SmartAPICallback<List<String>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.getMessage());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.getMessage());
			}

			@Override
			public void onSuccess(List<String> t) {
				System.out.println(t);
				if (t.size() > 0) {
					SmartAPI.confKeys(t.get(0)).run(new SmartAPICallback<List<String>>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.getMessage());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.getMessage());
						}

						@Override
						public void onSuccess(List<String> t2) {
							System.out.println(t);
							if (t2.size() > 0)
								SmartAPI.confGet(t.get(0), t2.get(0)).run(new SmartAPICallback<String>() {

									@Override
									public void onError(ServerStatus s) {
										Assert.fail(s.getMessage());
									}

									@Override
									public void onFail(Exception e) {
										Assert.fail(e.getMessage());
									}

									@Override
									public void onSuccess(String t) {
										System.out.println(t);
									}
								});
						}
					});
					SmartAPI.confSet(t.get(0), "test", "ITWORKS").run(null);
					SmartAPI.confGet(t.get(0), "test").run(new SmartAPICallback<String>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.getMessage());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.getMessage());
						}

						@Override
						public void onSuccess(String t) {
							System.out.println(t);
						}
					});
				}
			}
		});
	}

	@After
	public void tearDown() throws Exception {}
}
