package eip.smart.api;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.Unirest;

import eip.smart.cscommons.model.ServerStatus;
import eip.smart.cscommons.model.agent.Agent;
import eip.smart.cscommons.model.geometry.Point3D;

@SuppressWarnings("static-method")
public class SmartAPIFakeAgentTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SmartAPI.SERVER_URL = "http://localhost:8080/smartserver/";
		Unirest.setTimeouts(1000, 1000);
		SmartAPI.createTestAgents(5);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		SmartAPI.deleteTestAgents().run(null);
	}

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}

	@Test
	public void testAddAgentWithAlreadyAddedAgent() {
		String name = "testjunit01";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		// modeling creation
		SmartAPI.modelingCreate(name).run(null);

		// Adds agent 1 first time
		SmartAPI.agentAdd("TestAgent#1").run(null);

		// Try to add agent 1 a second time
		SmartAPI.agentAdd("TestAgent#1").run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use agentAdd with an agent already added to the current modelisation");
			}
		});
	}

	@Test
	public void testAddAgentWithNonExistingAgent() {
		String name = "testjunit01";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		// modeling creation
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.agentAdd("zeiofjpofjzepriofj").run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use agentAdd without a non existing agent");
			}
		});
	}

	@Test
	public void TestAgentAddWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		SmartAPI.agentAdd("TestAgent#1").run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use agentAdd and there is no current modeling");
			}
		});
	}

	@Test
	public void TestAgentRecallWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		SmartAPI.agentRecall("TestAgent#1").run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use agentRecall and there is no current modeling");
			}
		});
	}

	@Test
	public void TestAgentRemoveWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		SmartAPI.agentRemove("TestAgent#1").run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use agentRemove and there is no current modeling");
			}
		});
	}

	@Test
	public void TestManualOrderWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		Point3D p = new Point3D(0, 0, 0);

		SmartAPI.manualOrder("TestAgent#1", p).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occur when we use manualOrder and there is no current modeling");
			}
		});
	}

	@Test
	public void TestSimpleAgentAdd() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getAgentsAvailable().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				if (t.size() > 0)
					SmartAPI.agentAdd(t.get(0).getName()).run(new SmartAPICallback<ServerStatus>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.toString());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.toString());
						}

						@Override
						public void onSuccess(ServerStatus t) {
							Assert.assertTrue(true);
						}
					});
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleAgentRecall() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getAgentsAvailable().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				if (t.size() > 0) {
					SmartAPI.agentAdd(t.get(0).getName()).run(null);
					SmartAPI.agentRecall(t.get(0).getName()).run(new SmartAPICallback<ServerStatus>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.toString());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.toString());
						}

						@Override
						public void onSuccess(ServerStatus t) {
							Assert.assertTrue(true);
						}
					});
				}
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleAgentRemove() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getAgentsAvailable().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				if (t.size() > 0) {
					SmartAPI.agentAdd(t.get(0).getName()).run(null);
					SmartAPI.agentRemove(t.get(0).getName()).run(new SmartAPICallback<ServerStatus>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.toString());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.toString());
						}

						@Override
						public void onSuccess(ServerStatus t) {
							Assert.assertTrue(true);
						}
					});
				}
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleManualOrder() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.modelingStart().run(null);

		Point3D p = new Point3D(0, 0, 0);

		SmartAPI.getAgentsAvailable().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				if (t.size() > 0)
					SmartAPI.manualOrder(t.get(0).getName(), p).run(new SmartAPICallback<ServerStatus>() {

						@Override
						public void onError(ServerStatus s) {
							Assert.fail(s.toString());
						}

						@Override
						public void onFail(Exception e) {
							Assert.fail(e.toString());
						}

						@Override
						public void onSuccess(ServerStatus t) {
							Assert.assertTrue(true);
						}
					});
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}
}
