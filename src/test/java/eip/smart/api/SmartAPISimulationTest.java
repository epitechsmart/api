package eip.smart.api;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.Unirest;

import eip.smart.cscommons.model.ServerStatus;
import eip.smart.cscommons.model.modeling.Modeling;

@SuppressWarnings("static-method")
public class SmartAPISimulationTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SmartAPI.SERVER_URL = "http://localhost:8080/smartserver/";
		Unirest.setTimeouts(1000, 1000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Test
	public void ModelingSimulation() {
		String name = "modelingSimulation";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		SmartAPI.modelingCreate(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {}
		});

		// check que la mod�lisation courante est bien la mod�lisation cr��e
		SmartAPI.modelingInfo().run(new SmartAPICallback<Modeling>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(Modeling t) {
				if (!t.getName().equals(name))
					Assert.fail("error with the name of the modelisation");
			}
		});

		// arret de la mod�lisation
		SmartAPI.modelingStop().run(null);

		// check que la mod�lisation cr��e puis stop�e est bien pr�sente dans la liste des mod�lisations
		SmartAPI.modelingList().run(new SmartAPICallback<List<Modeling>>() {
			boolean	found	= false;

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Modeling> t) {
				Iterator<Modeling> it = t.iterator();
				while (it.hasNext()) {
					Modeling tmp = it.next();
					if (tmp.getName().equals(name))
						this.found = true;
				}
				if (!this.found)
					Assert.fail("modeling not on the list");
			}
		});
		SmartAPI.modelingDelete(name).run(null);
		Assert.assertTrue(true);
	}

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}
}
