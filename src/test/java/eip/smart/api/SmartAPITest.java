package eip.smart.api;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eip.smart.cscommons.model.ServerStatus;
import eip.smart.cscommons.model.agent.Agent;
import eip.smart.cscommons.model.geometry.Point3D;
import eip.smart.cscommons.model.geometry.polygon.Polygon;
import eip.smart.cscommons.model.modeling.Area;
import eip.smart.cscommons.model.modeling.Modeling;

@SuppressWarnings("static-method")
public class SmartAPITest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SmartAPI.SERVER_URL = "http://localhost:8080/smartserver/";
		SmartAPI.setTimeouts(1000, 1000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Test
	public void ComplexModelingSaveAndLoadTest() {
		Point3D point1 = new Point3D(1, 1, 1);
		Point3D point2 = new Point3D(2, 2, 2);

		Polygon pol1 = new Polygon();
		Polygon pol2 = new Polygon();

		pol1.add(point1.to2D());
		pol2.add(point2.to2D());

		eip.smart.cscommons.model.modeling.Area a1 = new eip.smart.cscommons.model.modeling.Area(pol1);
		eip.smart.cscommons.model.modeling.Area a2 = new eip.smart.cscommons.model.modeling.Area(pol2);

		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.addArea(a1);
		SmartAPI.addArea(a2);
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingLoad(name).run(null);

		SmartAPI.modelingInfo().run(new SmartAPICallback<Modeling>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(Modeling t) {
				// Area
				if (t.getAreas().size() != 2)
					Assert.fail("wrong number of areas");

				Assert.assertTrue(true);
			}
		});
	}

	@Test
	public void ComplexTestModelingCreate() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		SmartAPI.modelingCreate(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {}
		});

		// check que la mod�lisation courante est bien la mod�lisation cr��e
		SmartAPI.modelingInfo().run(new SmartAPICallback<Modeling>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(Modeling t) {
				if (!t.getName().equals(name))
					Assert.fail("error with the name of the modelisation");
			}
		});

		// arret de la mod�lisation
		SmartAPI.modelingStop().run(null);

		// check que la mod�lisation cr��e puis stop�e est bien pr�sente dans la liste des mod�lisations
		SmartAPI.modelingList().run(new SmartAPICallback<List<Modeling>>() {
			boolean	found	= false;

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Modeling> t) {
				Iterator<Modeling> it = t.iterator();
				while (it.hasNext()) {
					Modeling tmp = it.next();
					if (tmp.getName().equals(name))
						this.found = true;
				}
				if (!this.found)
					Assert.fail("modeling not on the list");
			}
		});
		SmartAPI.modelingDelete(name).run(null);
		Assert.assertTrue(true);
	}

	// simples tests servlets

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}

	@Test
	public void TestAddAreaWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		Area a = new Area();
		SmartAPI.addArea(a).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use addArea and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestGetPointsWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		SmartAPI.getPoints().run(new SmartAPICallback<List<Point3D>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Point3D> t) {
				Assert.fail("An error should occurre when we use getPoints and there is no current modelisation");
			}
		});
	}

	@Test
	public void testLoadModelisationWithNotExistingModeling() {
		String name = "testjunit01";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingLoad(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());

			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use loadModelisation with a non existing modelisation");
			}
		});
	}

	@Test
	public void TestModelingInfoWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingInfo().run(new SmartAPICallback<Modeling>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(Modeling t) {
				Assert.fail("An error should occurre when we use modelingInfo and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestModelingPauseWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingPause().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use modelingPause and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestModelingResumeWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingResume().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use modelingResume and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestModelingSaveWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingSave().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.assertTrue(true);
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use modelingSave and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestModelingStartWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingStart().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use modelingStart and there is no current modelisation");
			}
		});
	}

	@Test
	public void TestModelingStopWithNoCurrentModeling() {
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingStop().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use modelingStop and there is no current modelisation");
			}
		});
	}

	@Test
	public void testPauseModelingAlredyInPause() {
		String name = "testjunit01";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		// creation modelisation
		SmartAPI.modelingCreate(name).run(null);
		// lancement de la mod�lisation
		SmartAPI.modelingStart().run(null);
		// mise en pause de la mod�lisation
		SmartAPI.modelingPause().run(null);

		// check que la mod�lisation mise en pause ne peut pas �tre � nouveau mise en pause
		SmartAPI.modelingPause().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use ModelingPause and the current modelisation is already paused");
			}
		});
	}

	@Test
	public void testResumeModelingNotInPause() {
		String name = "testjunit01";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		// creation modelisation
		SmartAPI.modelingCreate(name).run(null);
		// lancement de la mod�lisation
		SmartAPI.modelingStart().run(null);
		SmartAPI.modelingResume().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.assertTrue(true);
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("An error should occurre when we use ModelingResume and the current modelisation is not in pause");
			}
		});
	}

	// tests servlets avec v�rifications

	// tests servlet sans Modelisation

	@Test
	public void testSameNameModelingCreate() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		// creation premiere modelisation
		SmartAPI.modelingCreate(name).run(null);
		// creation seconde modelisation avec le meme nom
		SmartAPI.modelingCreate(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.fail("we shouldn't be able to create 2 modelisations with the same name");
			}
		});

		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		Assert.assertTrue(true);
	}

	// @Test
	public void TestSimpleAddArea() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		eip.smart.cscommons.model.modeling.Area a = new eip.smart.cscommons.model.modeling.Area();
		SmartAPI.addArea(a).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleGetAgents() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getAgents().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleGetAgentsAvailable() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getAgentsAvailable().run(new SmartAPICallback<List<Agent>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Agent> t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleGetPoints() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.getPoints().run(new SmartAPICallback<List<Point3D>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				e.printStackTrace();
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<Point3D> t) {
				System.out.println(t);
				Assert.assertTrue(true);
			}
		});

		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleListStatus() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.listStatus().run(new SmartAPICallback<List<ServerStatus>>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(List<ServerStatus> t) {
				Assert.assertTrue(true);
				System.out.println(t);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingCreate() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);

		SmartAPI.modelingCreate(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingDelete() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	// test cas d'erreurs servlets

	@Test
	public void TestSimpleModelingInfo() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.modelingInfo().run(new SmartAPICallback<Modeling>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(Modeling t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingLoad() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.modelingStop().run(null);

		SmartAPI.modelingLoad(name).run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingPause() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.modelingStart().run(null);

		SmartAPI.modelingPause().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingResume() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);
		SmartAPI.modelingStart().run(null);
		SmartAPI.modelingPause().run(null);
		SmartAPI.modelingResume().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});

		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	// autres tests

	@Test
	public void TestSimpleModelingSave() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.modelingSave().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});

		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
	}

	@Test
	public void TestSimpleModelingStop() {
		String name = "testjunit";
		SmartAPI.modelingStop().run(null);
		SmartAPI.modelingDelete(name).run(null);
		SmartAPI.modelingCreate(name).run(null);

		SmartAPI.modelingStop().run(new SmartAPICallback<ServerStatus>() {

			@Override
			public void onError(ServerStatus s) {
				Assert.fail(s.toString());
			}

			@Override
			public void onFail(Exception e) {
				Assert.fail(e.toString());
			}

			@Override
			public void onSuccess(ServerStatus t) {
				Assert.assertTrue(true);
			}
		});
		SmartAPI.modelingDelete(name).run(null);
	}
}
